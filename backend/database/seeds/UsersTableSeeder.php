<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'Simple User';
        $user->email = 'simpleuser@gmail.com';
        $user->password = Hash::make('user123');
        $user->save();
    }
}
